# Setup

First, create a new file called `.env`. Place the following line in it:

```
PORT=8080
```

# Build and run the app with just Docker

To build the app, run
```
docker build -t <image name> .
```

To run the built image in a container, run
```
docker run -p 8080:8080 --env-file .env <image name>
```

Go to `localhost:8080`. You should see the "Hello World" message.

Note that if you `localhost:8080/users`, you will get a Connection Not Found error. More on that below.

# Build and run the App with docker-compose

The above docker commands can be simplified with docker-compose. To build the app, you can instead run
```
docker-compose -f docker-compose-1.yaml up
```

The file `docker-compose-1.yaml` abstracts away the need for specifying the ports and env file at the command line.

To stop your container, CNTRL+C, and then do `docker-compose down`.

# Build the app, network it with a database, and run it with docker-compose

To really see the power of docker-compose, let's add in a database as an additional service. Run
```
docker-compose -f docker-compose-2.yaml up
```

The file `docker-compose-2.yaml` contains a postgres service which is initialized with a `users` table and a single record.

Navigate to `localhost:8080/users`, and you should now see that record returned. 

When finished, do CNTRL+C and then do `docker-compose down`.