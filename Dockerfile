# to build: docker build -t <image-name> .
# to run: docker run -p 8080:8080 --env-file .env <image-name>

FROM node:10-alpine

# Create app directory
WORKDIR /usr/src/app

# Copy app dependency files to container
COPY package*.json tsconfig.json ./

# install dependencies on container
RUN npm install

# copy over src files
COPY ./src ./src

# compile
RUN npm run tsc

CMD [ "npm", "run", "start" ]