import { DBConnection } from "../../db/dbConnector";

export async function getUsers(res, db: DBConnection) {
    try {
        const users = await db.pool.query('SELECT * FROM users ORDER BY user_id ASC');
        return res.status(200).json(users.rows);
    }
    catch(error){
        return res.status(500).json(error);
    }
}