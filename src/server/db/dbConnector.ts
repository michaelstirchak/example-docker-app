import * as pg from 'pg';

export class DBConnection {
    public pool;

    constructor() {
        this.pool = new pg.Pool({
            user: "postgres",
            host: "db",
            database: 'postgres',
            password: "hello",
            port: 5432
        });
    }
}