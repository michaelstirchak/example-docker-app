import app from './app';
import * as bodyParser from 'body-parser';
import { getUsers } from './server/api/user/getUsers';
const port = process.env.PORT || 3000

app.express.use(bodyParser.json())
app.express.use(
  bodyParser.urlencoded({
    extended: true,
  })
)
app.express.get('/users', (req, res) => {
  return getUsers(res, app.db);
});

app.express.listen(port, (err) => {
  if (err) {
    return console.log(err)
  }

  return console.log(`server is listening on ${port}`)
})