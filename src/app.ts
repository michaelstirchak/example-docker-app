import * as express from 'express'
import * as dotenv from 'dotenv';
import { DBConnection } from './server/db/dbConnector';

dotenv.config();

class App {
  public express;
  public db;

  constructor () {
    this.express = express();
    this.db = new DBConnection();
    this.mountRoutes()
  }

  private mountRoutes (): void {
    const router = express.Router()
    router.get('/', (req, res) => {
      res.json({
        message: 'Hello World!'
      })
    })
    this.express.use('/', router)
  }
}

export default new App();